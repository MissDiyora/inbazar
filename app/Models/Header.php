<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Header extends Model
{
    use HasFactory;

    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 0;

    protected $table = 'header';

    protected $fillable = [
        'image',
        'title',
        'time_from',
        'time_till',
        'phone_one',
        'phone_second',
        'status'

    ];


}
