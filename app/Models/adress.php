<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Adress extends Model
{
    use HasFactory;

    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 0;

    protected $table = 'adress';

    protected $fillable = [
        'title',
        'description',
        'time_from',
        'time_till',
        'phone_one',
        'phone_two',
        'map_link',
        'created_at',
        'updated_at',
        'status'

    ];


}
