<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Network extends Model
{
    use HasFactory;

    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 0;

    const TYPE_INSTAGRAM = 1;
    const TYPE_FACEBOOK = 2;
    const TYPE_TELEGRAM = 3;
    const TYPE_YOUTUBE = 4;

    protected $table = 'network';

    protected $fillable = [
        'id',
        'type',
        'link',
        'created_at',
        'updated_at',
        'status'

    ];

    public function typeValue()
    {
        return self::typeList()[$this->type];
    }

    public static function typeList()
    {
        return [
            self::TYPE_INSTAGRAM => 'Instagram',
            self::TYPE_FACEBOOK => 'Facebook',
            self::TYPE_TELEGRAM => 'Telegram',
            self::TYPE_YOUTUBE => 'Youtube',
        ];
    }
}
