<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Info extends Model
{
    use HasFactory;

    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 0;

    protected $table = 'info';

    protected $fillable = [
        'title',
        'image',
        'info',
        'created_at',
        'updated_at',
        'status'

    ];


}
