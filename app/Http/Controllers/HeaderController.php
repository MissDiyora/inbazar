<?php

namespace App\Http\Controllers;

use App\Models\Header;
use Illuminate\Http\Request;

class HeaderController extends Controller
{
    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index()
    {
        $headers = Header::latest()->paginate(5);

        return view('header.index',compact('headers'));
    }

    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function create()
    {
        return view('header.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'image' => 'required',
            'title' => 'required',
            'time_from' => 'required',
            'time_till' => 'required',
            'phone_one' => 'required',
            'phone_second' => 'required',
        ]);

        Header::query()->where('status', '=', Header::STATUS_ACTIVE)->update([
            'status' => Header::STATUS_INACTIVE
        ]);

        if($request->file('image')){
            $file= $request->file('image');
            $filename= date('YmdHi').$file->getClientOriginalName();
            $file->move(public_path('public/image'), $filename);
            $data['image']= $filename;
        }

        Header::create([
            'image' => 'public/image/' . $filename,
            'title' => $request->title,
            'time_from' => $request->time_from,
            'time_till' => $request->time_till,
            'phone_one' => $request->phone_one,
            'phone_second' => $request->phone_second,
            'status' => Header::STATUS_ACTIVE,
        ]);

        return redirect()->route('header.index')
            ->with('success','Header created successfully.');
    }

    public function edit(Header $header)
    {

        return view('header.edit',compact('header'));
    }

    public function update(Request $request, Header $header)
    {
        $request->validate([
            'title' => 'required',
            'time_from' => 'required',
            'time_till' => 'required',
            'phone_one' => 'required',
            'phone_second' => 'required',
        ]);

        $header->update([
            'image' => $request->image ?? $header->image,
            'title' => $request->title,
            'time_from' => $request->time_from,
            'time_till' => $request->time_till,
            'phone_one' => $request->phone_one,
            'phone_second' => $request->phone_second,
        ]);

        return redirect()->route('header.index')
            ->with('success','Header updated successfully');
    }
}
