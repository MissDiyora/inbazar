<?php

namespace App\Http\Controllers;
use App\Models\Network;
use Illuminate\Http\Request;
use Carbon\Carbon;

class NetworkController extends Controller
{
    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index()
    {
        $network = Network::latest()->paginate(5);

        return view('network.index',compact('network'));
    }

    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function create()
    {
        return view('network.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'type' => 'required',
            'link' => 'required',
        ]);

        Network::query()->where('type', '=', $request->type)->delete();

        Network::create([

            'type' => $request->type,
            'link' => $request->link,
            'status' => Network::STATUS_ACTIVE,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        return redirect()->route('network.index')
            ->with('success','Network created successfully.');
    }

    public function edit(Network $network)
    {
        return view('network.edit',compact('network'));
    }

    public function update(Request $request, Network $network)
    {
        $request->validate([
            'type' => 'required',
            'link' => 'required',
            'status' => 'required',
        ]);


        $network->update([
            'type' => $request->type,
            'link' => $request->link,
            'status' => $request->status,
        ]);

        return redirect()->route('network.index')
            ->with('success','Network updated successfully');
    }
}

