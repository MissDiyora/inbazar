<?php

namespace App\Http\Controllers;
use App\Models\Info;
use Illuminate\Http\Request;
use Carbon\Carbon;
class InfoController extends Controller
{
    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index()
    {
        $info = Info::latest()->paginate(5);

        return view('info.index',compact('info'));
    }

    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function create()
    {
        return view('info.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required',
            'image' => 'required',
            'info' => 'required',
        ]);

        Info::query()->delete();

        if($request->file('image')){
            $file= $request->file('image');
            $filename= date('YmdHi').$file->getClientOriginalName();
            $file->move(public_path('public/image'), $filename);
            $data['image']= $filename;
        }

        Info::create([
            'image' => 'public/image/' . $filename,
            'title' => $request->title,
            'info' => $request->info,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        return redirect()->route('info.index')
            ->with('success','Info created successfully.');
    }

    public function edit(Info $info)
    {

        return view('info.edit',compact('info'));
    }

    public function update(Request $request, Info $info)
    {
        $request->validate([
            'title' => 'required',
            'info' => 'required',
        ]);

        if($request->file('image')){
            $file= $request->file('image');
            $filename= date('YmdHi').$file->getClientOriginalName();
            $file->move(public_path('public/image'), $filename);
            $data['image']= $filename;
            $filename = 'public/image/' . $filename;
        }

        $info->update([
            'image' => $filename ?? $info->image,
            'title' => $request->title,
            'info' => $request->info,
            'updated_at' => Carbon::now(),
        ]);

        return redirect()->route('info.index')
            ->with('success','Info updated successfully');
    }
}

