<?php

namespace App\Http\Controllers;

use App\Models\Adress;
use App\Models\Header;
use App\Models\Info;
use App\Models\Network;

class SiteController extends Controller
{
    public function index()
    {
        $header = Header::query()->orderByDesc('id')->first();
        $info = Info::query()->orderByDesc('id')->first();
        $network = [
            'instagram' => Network::query()->where('type', '=', Network::TYPE_INSTAGRAM)->orderByDesc('id')->first(),
            'facebook' => Network::query()->where('type', '=', Network::TYPE_FACEBOOK)->orderByDesc('id')->first(),
            'telegram' => Network::query()->where('type', '=', Network::TYPE_TELEGRAM)->orderByDesc('id')->first(),
            'youtube' => Network::query()->where('type', '=', Network::TYPE_YOUTUBE)->orderByDesc('id')->first(),
        ];
        $addresses = Adress::query()->get();

        return view('site.index', [
            'header' => $header,
            'info' => $info,
            'network' => $network,
            'addresses' => $addresses,
        ]);
    }
}
