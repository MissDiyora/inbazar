<?php

namespace App\Http\Controllers;
use App\Models\Adress;
use App\Models\Network;
use Illuminate\Http\Request;
use Carbon\Carbon;

class AdressController extends Controller
{
    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index()
    {
        $adress = Adress::latest()->paginate(5);

        return view('adress.index',compact('adress'));
    }

    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function create()
    {
        return view('adress.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required',
            'description' => 'required',
            'time_from' => 'required',
            'time_till' => 'required',
            'phone_one' => 'required',
            'phone_two' => 'required',
            'map_link' => 'required',
        ]);

        Adress::create([
            'title' => $request->title,
            'description' => $request->description,
            'time_from' => $request->time_from,
            'time_till' => $request->time_till,
            'phone_one' => $request->phone_one,
            'phone_two' => $request->phone_two,
            'map_link' => $request->map_link,
            'status' => Adress::STATUS_ACTIVE,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        return redirect()->route('adress.index')
            ->with('success', 'Adress created successfully.');
    }

    public function edit(Adress $adress)
    {

        return view('adress.edit',compact('adress'));
    }

    public function update(Request $request, Adress $adress)
    {
        $request->validate([
            'title' => 'required',
            'description' => 'required',
            'time_from' => 'required',
            'time_till' => 'required',
            'phone_one' => 'required',
            'phone_two' => 'required',
            'map_link' => 'required',
        ]);

        $adress->update([
            'title' => $request->title,
            'description' => $request->description,
            'time_from' => $request->time_from,
            'time_till' => $request->time_till,
            'phone_one' => $request->phone_one,
            'phone_two' => $request->phone_two,
            'map_link' => $request->map_link,
            'updated_at' => Carbon::now(),
        ]);

        return redirect()->route('adress.index')
            ->with('success','Adress updated successfully');
    }
}

