<?php

use App\Http\Controllers\AdressController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\InfoController;
use App\Http\Controllers\NetworkController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HeaderController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [App\Http\Controllers\SiteController::class, 'index']);

Route::get('/login', function () {
    return redirect()->route('login');
});

Auth::routes(['register' => false]);

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

// Profile Routes
Route::prefix('profile')->name('profile.')->middleware('auth')->group(function(){
    Route::get('/', [HomeController::class, 'getProfile'])->name('detail');
    Route::post('/update', [HomeController::class, 'updateProfile'])->name('update');
    Route::post('/change-password', [HomeController::class, 'changePassword'])->name('change-password');
});

// Header Routes
Route::prefix('header')->name('header.')->middleware('auth')->group(function(){
    Route::get('/', [HeaderController::class, 'index'])->name('index');
    Route::get('/create', [HeaderController::class, 'create'])->name('create');
    Route::post('/store', [HeaderController::class, 'store'])->name('store');
    Route::get('/edit/{header}', [HeaderController::class, 'edit'])->name('edit');
    Route::put('/update/{header}', [HeaderController::class, 'update'])->name('update');

});

// Info Routes
Route::prefix('info')->name('info.')->middleware('auth')->group(function(){
    Route::get('/index', [InfoController::class, 'index'])->name('index');
    Route::get('/create', [InfoController::class, 'create'])->name('create');
    Route::post('/store', [InfoController::class, 'store'])->name('store');
    Route::get('/edit/{info}', [InfoController::class, 'edit'])->name('edit');
    Route::put('/update/{info}', [InfoController::class, 'update'])->name('update');

});

// Network Routes
Route::prefix('network')->name('network.')->middleware('auth')->group(function(){
    Route::get('/index', [NetworkController::class, 'index'])->name('index');
    Route::get('/create', [NetworkController::class, 'create'])->name('create');
    Route::post('/store', [NetworkController::class, 'store'])->name('store');
    Route::get('/edit/{network}', [NetworkController::class, 'edit'])->name('edit');
    Route::put('/update/{network}', [NetworkController::class, 'update'])->name('update');

});

// Adress Routes
Route::prefix('adress')->name('adress.')->middleware('auth')->group(function(){
    Route::get('/index', [AdressController::class, 'index'])->name('index');
    Route::get('/create', [AdressController::class, 'create'])->name('create');
    Route::post('/store', [AdressController::class, 'store'])->name('store');
    Route::get('/edit/{adress}', [AdressController::class, 'edit'])->name('edit');
    Route::put('/update/{adress}', [AdressController::class, 'update'])->name('update');

});



// Roles
Route::resource('roles', App\Http\Controllers\RolesController::class);

// Permissions
Route::resource('permissions', App\Http\Controllers\PermissionsController::class);

// Users
Route::middleware('auth')->prefix('users')->name('users.')->group(function(){
    Route::get('/', [UserController::class, 'index'])->name('index');
    Route::get('/create', [UserController::class, 'create'])->name('create');
    Route::post('/store', [UserController::class, 'store'])->name('store');
    Route::get('/edit/{user}', [UserController::class, 'edit'])->name('edit');
    Route::put('/update/{user}', [UserController::class, 'update'])->name('update');
    Route::delete('/delete/{user}', [UserController::class, 'delete'])->name('destroy');
    Route::get('/update/status/{user_id}/{status}', [UserController::class, 'updateStatus'])->name('status');


    Route::get('/import-users', [UserController::class, 'importUsers'])->name('import');
    Route::post('/upload-users', [UserController::class, 'uploadUsers'])->name('upload');

    Route::get('export/', [UserController::class, 'export'])->name('export');

});

