<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="{{ asset('template/css/main.css') }}">
    <title>InBazar</title>
</head>

<body>
<header class="header">
    <div class="container">
        <nav class="nav">
            <div class="nav-logo">
                <img src="{{ asset($header->image) }}" alt="">
            </div>
            <div class="nav-info">
                <div>
                    <p>{{ $header->title }}</p>
                    <p>
                        <img src="{{ asset('template/assets/icons/oclock.svg') }}" alt="">
                        <span class="working-time">Ish vaqti</span><span>{{ $header->time_from }}-{{ $header->time_till }}</span></p>
                </div>
                <div>
                    <p>
                        <img src="{{ asset('template/assets/icons/phone.svg') }}" alt="">
                        <a href="tel:998970138888">{{ $header->phone_one }}</a>
                    </p>
                    <p>
                        <img src="{{ asset('template/assets/icons/phone.svg') }}" alt="">
                        <a href="tel:998909469494">{{ $header->phone_second }}</a>
                    </p>
                </div>
            </div>
        </nav>
    </div>
</header>
<main>
    <section>
        <div class="container">
            <div class="hero-container">
                <div class="hero-info">
                    <h1 class="hero-heading">
                        {{ $info->title }}
                    </h1>
                    <p class="hero-text">
                        {{ $info->info }}
                    </p>
                </div>
                <div class="hero-image">
                    <img src="{{ asset($info->image) }}" alt="">
                </div>
            </div>
        </div>
    </section>
    <section class="contact-section">
        <div class="container">
            <div class="contact-container">
                <ul class="contact-list">
                    <li class="contact-item">
                        <a href="{{ $network['instagram']->link }}" target="_blank" class="contact-link instagram">
                            <span class="contact-link__img"><img
                                    src="{{ asset('template/assets/icons/instagram.svg') }}" alt=""></span> <span
                                class="contact-link__text">Instagram</span>
                        </a>
                    </li>
                    <li class="contact-item ">
                        <a href="{{ $network['facebook']->link }}" target="_blank" class="contact-link facebook">
                            <span class="contact-link__img"><img src="{{ asset('template/assets/icons/facebook.svg') }}"
                                                                 alt=""></span> <span class="contact-link__text">Facebook</span>
                        </a>
                    </li>
                    <li class="contact-item ">
                        <a href="{{ $network['telegram']->link }}" target="_blank" class="contact-link telegram">
                            <span class="contact-link__img"><img src="{{ asset('template/assets/icons/telegram.svg') }}"
                                                                 alt=""></span> <span class="contact-link__text">Telegram</span>
                        </a>
                    </li>
                    <li class="contact-item ">
                        <a href="{{ $network['youtube']->link }}" target="_blank" class="contact-link youtube">
                            <span class="contact-link__img"><img src="{{ asset('template/assets/icons/youtube.svg') }}"
                                                                 alt=""></span> <span
                                class="contact-link__text">Youtube</span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </section>
    <section>
        <div class="container">
            <div class="location-container">
                <h1 class="location-heading">Bizning manzil</h1>
                <ul class="location-list">
                    @foreach($addresses as $address)
                        <li class="location-item">
                        <div class="circle">
                            <img src="{{ asset('template/assets/icons/location.svg') }}" alt="">
                        </div>
                        <h3 class="branch">
                            {{ $address->title }}
                        </h3>
                        <p class="branch location">
                            {{ $address->description }}
                        </p>
                        <p class="branch">
                            Ish vaqti har kuni
                            <span>{{ $address->time_from }} dan {{ $address->time_till }} gacha</span>
                        </p>
                        <p class="branch">
                            Aloqadamiz
                        <div class="phone-wrapper">
                            <img src="{{ asset('template/assets/icons/tick.svg') }}" alt="">
                            <a href="tel:998909469494">{{ $address->phone_one }}</a>
                        </div>
                        <div class="phone-wrapper">
                            <img src="{{ asset('template/assets/icons/tick.svg') }}" alt="">
                            <a href="tel:998970138888">{{ $address->phone_two }}</a>
                        </div>
                        </p>
                        <a class="location-link" target="_blank" href="{{ $address->map_link }}">
                            Xaritada ko'rish
                        </a>
                    </li>
                    @endforeach
                </ul>
            </div>
        </div>
    </section>
</main>
</body>

</html>
