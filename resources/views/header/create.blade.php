@extends('layouts.app')

@section('title', 'Add Header')

@section('content')


    <div class="container-fluid">

        <!-- Page Heading -->
        <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Add header</h1>
            <a href="{{route('header.index')}}" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i
                    class="fas fa-arrow-left fa-sm text-white-50"></i> Back</a>
        </div>

        {{-- Alert Messages --}}
        @include('common.alert')

        <!-- DataTales Example -->
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Add New Header</h6>
            </div>
            <form method="POST" action="{{route('header.store')}}" enctype='multipart/form-data'>
                @csrf
                <div class="card-body">
                    <div class="form-group row">

                        {{-- First Name --}}
                        <div class="col-sm-6 mb-3 mt-3 mb-sm-0">
                            <label><span style="color:red;">*</span>Title</label>
                            <input
                                type="text"
                                class="form-control form-control-user @error('title') is-invalid @enderror"
                                id="exampleTitle"
                                placeholder="Title"
                                name="title"
                                value="{{ old('title') }}">

                            @error('title')
                            <span class="text-danger">{{$message}}</span>
                            @enderror
                        </div>

                        <div class="col-sm-6 mb-3 mt-3 mb-sm-0">
                            <label><span style="color:red;">*</span>Image</label>
                            <input
                                type="file"
                                class="form-control form-control-user @error('image') is-invalid @enderror"
                                id="exampleImage"
                                placeholder="Image"
                                name="image"
                                value="{{ old('image') }}">

                            @error('image')
                            <span class="text-danger">{{$message}}</span>
                            @enderror
                        </div>

                        <div class="col-sm-6 mb-3 mt-3 mb-sm-0">
                            <label><span style="color:red;">*</span>Time from</label>
                            <input
                                type="time"
                                class="form-control form-control-user @error('time_from') is-invalid @enderror"
                                id="exampleTimeFrom"
                                name="time_from"
                                value="{{ old('time_from') }}">

                            @error('time_from')
                            <span class="text-danger">{{$message}}</span>
                            @enderror
                        </div>

                        <div class="col-sm-6 mb-3 mt-3 mb-sm-0">
                            <label><span style="color:red;">*</span>Time till</label>
                            <input
                                type="time"
                                class="form-control form-control-user @error('time_till') is-invalid @enderror"
                                id="exampleTimeFrom"
                                name="time_till"
                                value="{{ old('time_till') }}">

                            @error('time_till')
                            <span class="text-danger">{{$message}}</span>
                            @enderror
                        </div>

                        <div class="col-sm-6 mb-3 mt-3 mb-sm-0">
                            <label><span style="color:red;">*</span>Phone (1)</label>
                            <input
                                type="text"
                                class="form-control form-control-user @error('phone_one') is-invalid @enderror"
                                id="examplePhoneOne"
                                placeholder="Phone"
                                name="phone_one"
                                value="{{ old('phone_one') }}">

                            @error('phone_one')
                            <span class="text-danger">{{$message}}</span>
                            @enderror
                        </div>

                        <div class="col-sm-6 mb-3 mt-3 mb-sm-0">
                            <label><span style="color:red;">*</span>Phone (2)</label>
                            <input
                                type="text"
                                class="form-control form-control-user @error('phone_second') is-invalid @enderror"
                                id="examplePhoneSecond"
                                placeholder="Phone"
                                name="phone_second"
                                value="{{ old('phone_second') }}">

                            @error('phone_second')
                            <span class="text-danger">{{$message}}</span>
                            @enderror
                        </div>


                    </div>
                </div>

                <div class="card-footer">
                    <button type="submit" class="btn btn-success btn-user float-right mb-3">Save</button>
                    <a class="btn btn-primary float-right mr-3 mb-3" href="{{ route('header.index') }}">Cancel</a>
                </div>
            </form>
        </div>

    </div>
@endsection
