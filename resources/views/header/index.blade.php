@extends('layouts.app')

@section('title', 'Header List')

@section('content')
    <div class="container-fluid">

        <!-- Page Heading -->
        <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Header</h1>
            <div class="row">
                <div class="col-md-12">
                    <a href="{{ route('header.create') }}" class="btn btn-sm btn-primary">
                        <i class="fas fa-plus"></i> Add New
                    </a>
                </div>

            </div>

        </div>

        {{-- Alert Messages --}}
        @include('common.alert')

        <!-- DataTales Example -->
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">All Headers</h6>

            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                        <tr>
                            <th width="20%">Title</th>
                            <th width="20%">Image</th>
                            <th width="20%">Time</th>
                            <th width="20%">Phone</th>
                            <th width="15%">Status</th>
                            <th width="10%">Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($headers as $header)
                            <tr>
                                <td>{{ $header->title }}</td>
                                <td><img src="/{{ $header->image }}" alt="" style="width: 150px"></td>
                                <td>{{ $header->time_from }} <br> {{ $header->time_till }}</td>
                                <td>{{ $header->phone_one }} <br> {{ $header->phone_second }}</td>
                                <td>
                                    @if ($header->status == 0)
                                        <span class="badge badge-danger">Inactive</span>
                                    @elseif ($header->status == 1)
                                        <span class="badge badge-success">Active</span>
                                    @endif
                                </td>
                                <td>
                                    <a href="{{ route('header.edit', $header) }}"
                                       class="btn btn-primary m-2">
                                        <i class="fa fa-pen"></i>
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>

                    {{ $headers->links() }}
                </div>
            </div>
        </div>

    </div>


@endsection

@section('scripts')

@endsection
