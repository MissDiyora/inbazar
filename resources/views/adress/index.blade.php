@extends('layouts.app')

@section('title', 'adress List')

@section('content')
    <div class="container-fluid">

        <!-- Page Heading -->
        <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">adress</h1>
            <div class="row">
                <div class="col-md-12">
                    <a href="{{ route('adress.create') }}" class="btn btn-sm btn-primary">
                        <i class="fas fa-plus"></i> Add New
                    </a>
                </div>

            </div>

        </div>

        {{-- Alert Messages --}}
        @include('common.alert')

        <!-- DataTales Example -->
        <div class="card shadow mb-4">
            <div class="card-adress py-3">
                <h6 class="m-0 font-weight-bold text-primary">All adresses</h6>

            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                        <tr>
                            <th width="20%">Title</th>
                            <th width="20%">Description</th>
                            <th width="20%">Time</th>
                            <th width="20%">Phone</th>
                            <th width="15%">Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($adress as $add)
                            <tr>
                                <td>{{ $add->title }}</td>
                                <td>{{ $add->description }}</td>
                                <td>{{ $add->time_from }} |{{ $add->time_till }}</td>
                                <td>{{ $add->phone_one }} | {{ $add->phone_two }}</td>
                                <td>
                                    <a href="{{ route('adress.edit', $add) }}"
                                       class="btn btn-primary m-2">
                                        <i class="fa fa-pen"></i>
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>

                    {{ $adress->links() }}
                </div>
            </div>
        </div>

    </div>


@endsection

@section('scripts')

@endsection
