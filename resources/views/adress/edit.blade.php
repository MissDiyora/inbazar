@extends('layouts.app')

@section('title', 'Edit Adress')

@section('content')

    <div class="container-fluid">

        <!-- Page Heading -->
        <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Edit Adresss</h1>
            <a href="{{route('adress.index')}}" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i
                    class="fas fa-arrow-left fa-sm text-white-50"></i> Back</a>
        </div>

        {{-- Alert Messages --}}
        @include('common.alert')

        <!-- DataTales Example -->
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Edit Adress</h6>
            </div>
            <form method="POST" action="{{route('adress.update', $adress)}}">
                @csrf
                @method('PUT')

                <div class="card-body">
                    <div class="form-group row">

                        {{-- Last Name --}}
                        <div class="col-sm-6 mb-3 mt-3 mb-sm-0">
                            <label><span style="color:red;">*</span>Title</label>
                            <input
                                type="text"
                                class="form-control form-control-user @error('title') is-invalid @enderror"
                                id="exampleLastName"
                                placeholder="Title"
                                name="title"
                                value="{{ old('title') ? old('title') : $adress->title }}">

                            @error('title')
                            <span class="text-danger">{{$message}}</span>
                            @enderror
                        </div>

                        <div class="col-sm-6 mb-3 mt-3 mb-sm-0">
                            <label><span style="color:red;">*</span>Description</label>
                            <input
                                type="text"
                                class="form-control form-control-user @error('Description') is-invalid @enderror"
                                id="exampleDescription"
                                placeholder=" Description"
                                name="description"
                                value="{{ old('description') ?  old('description') : $adress->description}}">

                            @error('description')
                            <span class="text-danger">{{$message}}</span>
                            @enderror
                        </div>

                        <div class="col-sm-6 mb-3 mt-3 mb-sm-0">
                            <label><span style="color:red;">*</span>Time from</label>
                            <input
                                type="time"
                                class="form-control form-control-user @error('time_from') is-invalid @enderror"
                                id="exampleTime_from"
                                placeholder="Time_from"
                                name="time_from"
                                value="{{ old('time_from') ? old('time_from') : $adress->time_from }}">

                            @error('time_from')
                            <span class="text-danger">{{$message}}</span>
                            @enderror
                        </div>

                        <div class="col-sm-6 mb-3 mt-3 mb-sm-0">
                            <label><span style="color:red;">*</span>Time till</label>
                            <input
                                type="time"
                                class="form-control form-control-user @error('time_till') is-invalid @enderror"
                                id="exampleMobile"
                                placeholder="Time_till"
                                name="time_till"
                                value="{{ old('time_till') ? old('time_till') : $adress->time_till }}">

                            @error('time_till')
                            <span class="text-danger">{{$message}}</span>
                            @enderror
                        </div>

                        <div class="col-sm-6 mb-3 mt-3 mb-sm-0">
                            <label><span style="color:red;">*</span>Phone (1)</label>
                            <input
                                type="text"
                                class="form-control form-control-user @error('phone_one') is-invalid @enderror"
                                id="exampleMobile"
                                placeholder="Phone_one"
                                name="phone_one"
                                value="{{ old('phone_one') ? old('phone_one') : $adress->phone_one }}">

                            @error('phone_one')
                            <span class="text-danger">{{$message}}</span>
                            @enderror
                        </div>

                        <div class="col-sm-6 mb-3 mt-3 mb-sm-0">
                            <label><span style="color:red;">*</span>Phone (2)</label>
                            <input
                                type="text"
                                class="form-control form-control-user @error('phone_two') is-invalid @enderror"
                                id="exampleMobile"
                                placeholder="Phone"
                                name="phone_two"
                                value="{{ old('phone_two') ? old('phone_two') : $adress->phone_two }}">

                            @error('phone_two')
                            <span class="text-danger">{{$message}}</span>
                            @enderror
                        </div>
                        <div class="col-sm-6 mb-3 mt-3 mb-sm-0">
                            <label><span style="color:red;">*</span>Map link</label>
                            <input
                                type="text"
                                class="form-control form-control-user @error('map_link') is-invalid @enderror"
                                id="exampleMap_link"
                                placeholder="Map_link"
                                name="map_link"
                                value="{{ old('map_link') ? old('map_link') : $adress->map_link }}">

                            @error('map_link')
                            <span class="text-danger">{{$message}}</span>
                            @enderror
                        </div>

                    </div>
                </div>

                <div class="card-footer">
                    <button type="submit" class="btn btn-success btn-user float-right mb-3">Update</button>
                    <a class="btn btn-primary float-right mr-3 mb-3" href="{{ route('header.index') }}">Cancel</a>
                </div>
            </form>
        </div>

    </div>


@endsection
