@extends('layouts.app')

@section('title', 'Network List')

@section('content')
    <div class="container-fluid">

        <!-- Page Heading -->
        <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Network</h1>
            <div class="row">
                <div class="col-md-12">
                    <a href="{{ route('network.create') }}" class="btn btn-sm btn-primary">
                        <i class="fas fa-plus"></i> Add New
                    </a>
                </div>

            </div>

        </div>

        {{-- Alert Messages --}}
        @include('common.alert')

        <!-- DataTales Example -->
        <div class="card shadow mb-4">
            <div class="card-network py-3">
                <h6 class="m-0 font-weight-bold text-primary">All Networks</h6>

            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                        <tr>
                            <th width="20%">Type</th>
                            <th width="20%">Link</th>
                            <th width="20%">Created_at</th>
                            <th width="20%">Updated_at</th>
                            <th width="15%">Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($network as $nets)
                            <tr>
                                <td>{{ $nets->typeValue() }}</td>
                                <td>{{ $nets->link }}</td>
                                <td>{{ $nets->created_at }}</td>
                                <td>{{ $nets->updated_at }}</td>
                                <td>
                                    <a href="{{ route('network.edit', $nets) }}"
                                       class="btn btn-primary m-2">
                                        <i class="fa fa-pen"></i>
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>

                    {{ $network->links() }}
                </div>
            </div>
        </div>

    </div>


@endsection

@section('scripts')

@endsection
