@extends('layouts.app')

@section('title', 'Edit Network')

@section('content')

    <div class="container-fluid">

        <!-- Page Heading -->
        <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Edit Networks</h1>
            <a href="{{route('network.index')}}" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i
                    class="fas fa-arrow-left fa-sm text-white-50"></i> Back</a>
        </div>

        {{-- Alert Messages --}}
        @include('common.alert')

        <!-- DataTales Example -->
        <div class="card shadow mb-4">
            <div class="card-network py-3">
                <h6 class="m-0 font-weight-bold text-primary">Edit Network</h6>
            </div>
            <form method="POST" action="{{route('network.update', $network)}}">

                @csrf
                @method('PUT')

                <div class="card-body">
                    <div class="form-group row">

                        {{-- Type --}}
                        <div class="col-sm-6 mb-3 mt-3 mb-sm-0">
                           <label><span style="color:red;">*</span>Type</label>
                            <select class="form-control form-control-user @error('type _name') is-invalid @enderror" name="type_name">
                                <option selected disabled>Select Type</option>
                                <option value="telegram" {{old('type') ? ((old('type') == 'telegram') ? 'selected' : '') : (($network->type == 'telegram') ? 'selected' : '')}}>Telegram</option>
                                <option value="facebook" {{old('type') ? ((old('type') == 'facebook') ? 'selected' : '') : (($network->type == 'facebook') ? 'selected' : '')}}>Facebook</option>
                                <option value="instagram" {{old('type') ? ((old('type') == 'instagram') ? 'selected' : '') : (($network->type == 'instagram') ? 'selected' : '')}}>Instagram</option>
                                <option value="youtebe" {{old('type') ? ((old('type') == 'youtebe') ? 'selected' : '') : (($network->type == 'youtebe') ? 'selected' : '')}}>Youtebe</option>
                            </select>
                            @error('type')
                            <span class="text-danger">{{$message}}</span>
                            @enderror
                        </div>

                        <div class="col-sm-6 mb-3 mt-3 mb-sm-0">
                            <label><span style="color:red;">*</span>Link</label>
                            <input
                                type="text"
                                class="form-control form-control-user @error('Link') is-invalid @enderror"
                                id="exampleLink"
                                placeholder=" Link"
                                name="link"
                                value="{{ old('link') ?  old('link') : $network->link}}">

                            @error('link')
                            <span class="text-danger">{{$message}}</span>
                            @enderror
                        </div>

                    </div>
                </div>

                <div class="card-footer">
                    <button type="submit" class="btn btn-success btn-user float-right mb-3">Update</button>
                    <a class="btn btn-primary float-right mr-3 mb-3" href="{{ route('network.index') }}">Cancel</a>
                </div>
            </form>
        </div>

    </div>


@endsection
