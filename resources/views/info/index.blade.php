@extends('layouts.app')

@section('title', 'Info List')

@section('content')
    <div class="container-fluid">

        <!-- Page Heading -->
        <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Info</h1>
            <div class="row">
                <div class="col-md-12">
                    <a href="{{ route('info.create') }}" class="btn btn-sm btn-primary">
                        <i class="fas fa-plus"></i> Add New
                    </a>
                </div>

            </div>

        </div>

        {{-- Alert Messages --}}
        @include('common.alert')

        <!-- DataTales Example -->
        <div class="card shadow mb-4">
            <div class="card-info py-3">
                <h6 class="m-0 font-weight-bold text-primary">All Infos</h6>

            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                        <tr>
                            <th width="20%">Title</th>
                            <th width="20%">Info</th>
                            <th width="20%">Image</th>
                            <th width="20%">Created_at</th>
                            <th width="20%">Updated_at</th>
                            <th width="15%">Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($info as $inf)
                            <tr>
                                <td>{{ $inf->title }}</td>
                                <td>{{ $inf->info }}</td>
                                <td><img src="/{{ $inf->image }}" alt="" style="width: 150px"></td>
                                <td>{{ $inf->created_at }}</td>
                                <td>{{ $inf->updated_at }}</td>
                                <td>
                                    <a href="{{ route('info.edit', $inf) }}"
                                       class="btn btn-primary m-2">
                                        <i class="fa fa-pen"></i>
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>

                    {{ $info->links() }}
                </div>
            </div>
        </div>

    </div>


@endsection

@section('scripts')

@endsection
