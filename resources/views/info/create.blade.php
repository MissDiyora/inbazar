@extends('layouts.app')

@section('title', 'Add Info')

@section('content')


    <div class="container-fluid">

        <!-- Page Heading -->
        <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Add info</h1>
            <a href="{{route('info.index')}}" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i
                    class="fas fa-arrow-left fa-sm text-white-50"></i> Back</a>
        </div>

        {{-- Alert Messages --}}
        @include('common.alert')

        <!-- DataTales Example -->
        <div class="card shadow mb-4">
            <div class="card-info py-3">
                <h6 class="m-0 font-weight-bold text-primary">Add New Info</h6>
            </div>
            <form method="POST" action="{{route('info.store')}}" enctype='multipart/form-data'>
                @csrf
                <div class="card-body">
                    <div class="form-group row">

                        {{-- First Name --}}
                        <div class="col-sm-6 mb-3 mt-3 mb-sm-0">
                            <label><span style="color:red;">*</span>Title</label>
                            <input
                                type="text"
                                class="form-control form-control-user @error('title') is-invalid @enderror"
                                id="exampleTitle"
                                placeholder="Title"
                                name="title"
                                value="{{ old('title') }}">

                            @error('info')
                            <span class="text-danger">{{$message}}</span>
                            @enderror
                        </div>
                        <div class="col-sm-6 mb-3 mt-3 mb-sm-0">
                            <label><span style="color:red;">*</span>Info</label>
                            <input
                                type="text"
                                class="form-control form-control-user @error('info') is-invalid @enderror"
                                id="exampleInfo"
                                placeholder="Info"
                                name="info"
                                value="{{ old('info') }}">

                            @error('info')
                            <span class="text-danger">{{$message}}</span>
                            @enderror
                        </div>

                        <div class="col-sm-6 mb-3 mt-3 mb-sm-0">
                            <label><span style="color:red;">*</span>Image</label>
                            <input
                                type="file"
                                class="form-control form-control-user @error('image') is-invalid @enderror"
                                id="exampleImage"
                                placeholder="Image"
                                name="image"
                                value="{{ old('image') }}">

                            @error('created_at')
                            <span class="text-danger">{{$message}}</span>
                            @enderror
                        </div>
                    </div>


                </div>

                <div class="card-footer">
                    <button type="submit" class="btn btn-success btn-user float-right mb-3">Save</button>
                    <a class="btn btn-primary float-right mr-3 mb-3" href="{{ route('info.index') }}">Cancel</a>
                </div>
            </form>
        </div>

    </div>
@endsection
