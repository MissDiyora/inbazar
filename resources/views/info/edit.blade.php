@extends('layouts.app')

@section('title', 'Edit Info')

@section('content')

    <div class="container-fluid">

        <!-- Page Heading -->
        <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Edit Infos</h1>
            <a href="{{route('info.index')}}" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i
                    class="fas fa-arrow-left fa-sm text-white-50"></i> Back</a>
        </div>

        {{-- Alert Messages --}}
        @include('common.alert')

        <!-- DataTales Example -->
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Edit Info</h6>
            </div>
            <form method="POST" action="{{route('info.update', $info)}}" enctype='multipart/form-data'>
                @csrf
                @method('PUT')

                <div class="card-body">
                    <div class="form-group row">

                        {{-- Last Name --}}
                        <div class="col-sm-6 mb-3 mt-3 mb-sm-0">
                            <label><span style="color:red;">*</span>Title</label>
                            <input
                                type="text"
                                class="form-control form-control-user @error('title') is-invalid @enderror"
                                id="exampleLastName"
                                placeholder="Title"
                                name="title"
                                value="{{ old('title') ? old('title') : $info->title }}">

                            @error('title')
                            <span class="text-danger">{{$message}}</span>
                            @enderror
                        </div>

                        <div class="col-sm-6 mb-3 mt-3 mb-sm-0">
                            <label><span style="color:red;">*</span>Image</label>
                            <input
                                type="file"
                                class="form-control form-control-user @error('Image') is-invalid @enderror"
                                id="exampleImage"
                                placeholder=" Image"
                                name="image"
                                value="{{ old('Image') ?  old('Image') : $info->Image}}">

                            @error('Image')
                            <span class="text-danger">{{$message}}</span>
                            @enderror
                        </div>

                        <div class="col-sm-6 mb-3 mt-3 mb-sm-0">
                            <label><span style="color:red;">*</span>Info</label>
                            <input
                                type="text"
                                class="form-control form-control-user @error('info') is-invalid @enderror"
                                id="exampleInfo"
                                placeholder="Info"
                                name="info"
                                value="{{ old('info') ? old('info') : $info->info }}">

                            @error('info')
                            <span class="text-danger">{{$message}}</span>
                            @enderror
                        </div>

                    </div>
                </div>

                <div class="card-footer">
                    <button type="submit" class="btn btn-success btn-user float-right mb-3">Update</button>
                    <a class="btn btn-primary float-right mr-3 mb-3" href="{{ route('header.index') }}">Cancel</a>
                </div>
            </form>
        </div>

    </div>


@endsection
